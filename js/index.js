var calendar_controller =
		{
			target_change: function (obj)
			{
				var self = this;
				if (!obj.disabled)
				{
					obj.disabled = true;
					now_loading.on();
					self.target_select_form_submit();
				}
			},
			month_move: function (obj, target_month)
			{
				var self = this;
				if (!obj.disabled)
				{
					obj.disabled = true;
					now_loading.on();
					target_select_form.tm.value = target_month;
					self.target_select_form_submit();
				}
			},
			target_select_form_submit: function ()
			{
				var self = this;
				var url = getBaseQuery('index', 'index');
				location.href = self.get_calender_controll_query(url);
			},
			get_calender_controll_query: function (url)
			{
				url += '&tc=' + target_select_form.tc.value;
				url += '&tm=' + target_select_form.tm.value;
				return url;
			}
		};

var target_maintenance_controller =
		{
			dialog: undefined,
			setup: function ()
			{
				var self = this;
				if (self.dialog == undefined)
				{
					self.dialog = document.getElementById('target_maintenance_dialog_div');
				}
				var draggable = new Draggable(self.dialog, document.getElementById('target_maintenance_dialog_title_div'));
				self.dialog.style.display = 'none';
				addEvent(window, 'resize', function ()
				{
					mask.set_size();
					target_maintenance_controller.set_size();
				});
			},
			get_target_csv: function ()
			{
				var self = this;
				var url = getBaseQuery('maintenance', 'gettargetcsv');
				var data = '';
				var xhr_controller = new XHRController();
				xhr_controller.send(url, 'POST', data, true, self.get_target_csv_result, env.content_type);
			},
			get_target_csv_result: function (response)
			{
				if (response.succeeded)
				{
					document.getElementById('target_maintenance_csv').value = response.data['csv'];
					target_maintenance_controller._show();
				} else
				{
					alert(response.data['message']);
					now_loading.off();
				}
			},
			show: function ()
			{
				now_loading.on();
				var self = this;
				self.get_target_csv();
			},
			_show: function ()
			{
				now_loading.off();
				mask.on();
				var self = this;
				self.dialog.style.display = 'block';
				self.set_size();
			},
			set_size: function ()
			{
				var self = this;
				var top = 10;
				var left = (document.documentElement.clientWidth / 2);
				left -= (self.dialog.clientWidth / 2);
				self.dialog.style.top = top + 'px';
				self.dialog.style.left = left + 'px';
			},
			get_dialog_data: function ()
			{
				var data = '';
				data += '&pw=' + target_maintenance_form.target_maintenance_password.value;
				data += '&csv=' + encodeURIComponent(target_maintenance_form.target_maintenance_csv.value);
				return data;
			},
			validation: function ()
			{
				var messages = '';
				if (target_maintenance_form.target_maintenance_password.value == '')
				{
					messages += '【パスワード】を入力してください。\n';
				}
				if (target_maintenance_form.target_maintenance_csv.value == '')
				{
					messages += '全ての予約対象を削除することはできません。\n';
				}
				if (messages != '')
				{
					alert(messages);
					return false;
				}
				return true;
			},
			save: function ()
			{
				var self = this;
				var url = getBaseQuery('maintenance', 'importtargetcsv');
				if (self.validation())
				{
					now_loading.on();
					var data = self.get_dialog_data();
					var xhr_controller = new XHRController();
					xhr_controller.send(url, 'POST', data, true, self.save_result, env.content_type);
				}
			},
			save_result: function (response)
			{
				if (response.succeeded)
				{
					alert(response.data['message']);
					calendar_controller.target_select_form_submit();
				} else
				{
					alert(response.data['message']);
					now_loading.off();
				}
			},
			close: function ()
			{
				var self = this;
				self.dialog.style.display = 'none';
				mask.off();
			}
		};

var input_dialog_controller =
		{
			dialog: undefined,
			continuous: false,
			reload_when_close: false,
			setup: function ()
			{
				var self = this;
				if (self.dialog == undefined)
				{
					self.dialog = document.getElementById('input_dialog_div');
				}
				var draggable = new Draggable(self.dialog, document.getElementById('input_dialog_title_div'));
				self.dialog.style.display = 'none';
				addEvent(window, 'resize', function ()
				{
					mask.set_size();
					input_dialog_controller.set_size();
				});
			},
			modify: function (id)
			{
				now_loading.on();
				var self = this;
				self.get_booking_data(id);
			},
			get_booking_data: function (id)
			{
				var self = this;
				var url = getBaseQuery('index', 'get');
				var data = '&id=' + id;
				var xhr_controller = new XHRController();
				xhr_controller.send(url, 'POST', data, true, self.get_booking_data_result, env.content_type);
			},
			get_booking_data_result: function (response)
			{
				if (response.succeeded)
				{
					input_dialog_controller.input_modify(response.data['data']);
				} else
				{
					alert(response.data['message']);
					calendar_controller.target_select_form_submit();
				}
			},
			input_modify: function (data)
			{
				now_loading.on();
				var self = this;
				booking_form.booking_id.value = data['id'];
				booking_form.booking_update_datetime.value = data['update_datetime'];
				booking_form.booking_date.value = data['start_datetime'].left(10);
				booking_form.booking_start_time.value = data['start_datetime'].right(5);
				booking_form.booking_end_time.value = data['end_datetime'].right(5);
				booking_form.booking_title.value = data['title'];
				booking_form.booking_user_name.value = data['user_name'];
				// booking_form.booking_user_section_name.value = data['user_section_name'];
				// booking_form.booking_user_phone_number.value = data['user_phone_number'];
				// booking_form.booking_member_count.value = data['member_count'];
				booking_form.booking_description.value = data['description'];
				document.getElementById('booking_mode_display').innerText =
						'予約内容照会　(最終更新日時：' + data['update_datetime'] + ')';
				document.getElementById('input_dialog_save_button').innerText = '変更';
				// document.getElementById('input_dialog_cancel_button').style.display = 'inline-block';
				// document.getElementById('input_dialog_copy_button').style.display = 'inline-block';
				// document.getElementById('input_dialog_save_continuous_button').style.display = 'none';
				self.show();
				now_loading.off();
			},
			input_new: function (start_datetime)
			{
				now_loading.on();
				var self = this;
				booking_form.booking_id.value = '';
				booking_form.booking_update_datetime.value = '';
				booking_form.booking_date.value = start_datetime.left(10);
				booking_form.booking_start_time.value = start_datetime.right(5);
				booking_form.booking_end_time.selectedIndex = booking_form.booking_start_time.selectedIndex;
				booking_form.booking_title.value = '';
				booking_form.booking_user_name.value = '';
				// booking_form.booking_user_section_name.value = '';
				// booking_form.booking_user_phone_number.value = '';
				// booking_form.booking_member_count.selectedIndex = 0;
				booking_form.booking_description.value = '';
				document.getElementById('booking_mode_display').innerText = '新規予約';
				document.getElementById('input_dialog_save_button').innerText = '予約';
				// document.getElementById('input_dialog_cancel_button').style.display = 'none';
				// document.getElementById('input_dialog_copy_button').style.display = 'none';
				// document.getElementById('input_dialog_save_continuous_button').style.display = 'inline-block';
				self.show();
				now_loading.off();
			},
			copy: function ()
			{
				now_loading.on();
				var self = this;
				booking_form.booking_id.value = '';
				booking_form.booking_update_datetime.value = '';
				document.getElementById('booking_mode_display').innerText = '新規予約';
				document.getElementById('input_dialog_save_button').innerText = '予約';
				document.getElementById('input_dialog_cancel_button').style.display = 'none';
				// document.getElementById('input_dialog_copy_button').style.display = 'none';
				// document.getElementById('input_dialog_save_continuous_button').style.display = 'inline-block';
				self.show();
				now_loading.off();
			},
			show: function ()
			{
				mask.on();
				var self = this;
				self.dialog.style.display = 'block';
				self.set_size();
			},
			set_size: function ()
			{
				var self = this;
				var top = 10;
				var left = (document.documentElement.clientWidth / 2);
				left -= (self.dialog.clientWidth / 2);
				self.dialog.style.top = top + 'px';
				self.dialog.style.left = left + 'px';
			},
			close: function ()
			{
				var self = this;
				if (self.reload_when_close)
				{
					calendar_controller.target_select_form_submit();
				} else
				{
					self.dialog.style.display = 'none';
					mask.off();
				}
			},
			get_dialog_data: function ()
			{
				var data = '';
				data += '&id=' + booking_form.booking_id.value;
				data += '&update_datetime=' + encodeURIComponent(booking_form.booking_update_datetime.value);
				data +=
						'&start_datetime='
						+ encodeURIComponent(booking_form.booking_date.value + ' ' + booking_form.booking_start_time.value);
				data +=
						'&end_datetime='
						+ encodeURIComponent(booking_form.booking_date.value + ' ' + booking_form.booking_end_time.value);
				data += '&title=' + encodeURIComponent(booking_form.booking_title.value);
				data += '&user_name=' + encodeURIComponent(booking_form.booking_user_name.value);
				// data += '&user_section_name=' + encodeURIComponent(booking_form.booking_user_section_name.value);
				// data += '&user_phone_number=' + encodeURIComponent(booking_form.booking_user_phone_number.value);
				// data += '&member_count=' + booking_form.booking_member_count.value;
				data += '&description=' + encodeURIComponent(booking_form.booking_description.value);
				return data;
			},
			validation: function ()
			{
				var messages = '';
				if (booking_form.booking_start_time.value >= booking_form.booking_end_time.value)
				{
					messages += '【利用日時】を正しく入力してください。\n';
				}
				if (booking_form.booking_title.value == '')
				{
					messages += '【タイトル】を入力してください。\n';
				}
				if (booking_form.booking_user_name.value == '')
				{
					messages += '【予約者名】を入力してください。\n';
				}
				if (messages != '')
				{
					alert(messages);
					return false;
				}
				return true;
			},
			save_continuous: function ()
			{
				var self = this;
				self.continuous = true;
				self.reload_when_close = true;
				self.save();
			},
			save: function ()
			{
				var self = this;
				var url = getBaseQuery('index', 'input');
				url = calendar_controller.get_calender_controll_query(url);
				if (self.validation())
				{
					now_loading.on();
					var data = self.get_dialog_data();
					var xhr_controller = new XHRController();
					xhr_controller.send(url, 'POST', data, true, self.save_result, env.content_type);
				}
			},
			save_result: function (response)
			{
				if (response.succeeded)
				{
					alert(response.data['message']);
					if (!input_dialog_controller.continuous)
					{
						calendar_controller.target_select_form_submit();
					} else
					{
						now_loading.off();
					}
				} else
				{
					alert(response.data['message']);
					now_loading.off();
				}
				input_dialog_controller.continuous = false;
			},
			cancel: function ()
			{
				var self = this;
				var url = getBaseQuery('index', 'cancel');
				url = calendar_controller.get_calender_controll_query(url);
				if (confirm('本当にこの予約をキャンセルしますか？'))
				{
					now_loading.on();
					var data = self.get_dialog_data();
					var xhr_controller = new XHRController();
					xhr_controller.send(url, 'POST', data, true, self.save_result, env.content_type);
				}
			},
			cancel_result: function (response)
			{
				if (response.succeeded)
				{
					alert(response.data['message']);
					calendar_controller.target_select_form_submit();
				} else
				{
					alert(response.data['message']);
					now_loading.off();
				}
			}

		};

addEvent(window, 'load', function ()
{
	input_dialog_controller.setup();
	// target_maintenance_controller.setup();
});
