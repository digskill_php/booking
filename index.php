<?php

/**
 * このアプリケーションのエントリーポイントです。
 *
 * 全ての処理はここから始まります。ここでは基本的な定義と<br />
 * 設定ファイル（app.ini）の読み込みを行い<br />
 * 後のことはすべてDispatcher->dispatch()に処理を引き継ぎます。
 *
 * @copyright Copyright (c) 2019 WEM Co., Ltd. All Rights Reserved
 * @license https://opensource.org/licenses/mit-license.html MIT License
 */
date_default_timezone_set('Asia/Tokyo');

// このデバッグモードは「application/views/env.phtml」で
// javascriptにも引き継がれます。
define('DEBUG', true);

// このアプリケーションのタイトルです。
define('APP_TITLE', '予約システム');

// 物理パスに関する定数です。
define('BASE_DIR', dirname(__FILE__) . '/');
define('APP_DIR', './application/');
define('CTRL_DIR', APP_DIR . 'controllers/');
define('MODEL_DIR', APP_DIR . 'models/');
define('VIEW_DIR', APP_DIR . 'views/');
define('DB_DIR', APP_DIR . 'db/');
define('COMMON_DIR', APP_DIR . 'common/');

// URLに関する定数です。
define('APP_ID', basename(dirname(__FILE__)));
define('BASE_URL', dirname(filter_input(INPUT_SERVER, 'PHP_SELF')) . '/');
define('INDEX_URL', BASE_URL . 'index.php');
define('CSS_DIR', BASE_URL . 'css/');
define('JS_DIR', BASE_URL . 'js/');
define('IMG_DIR', CSS_DIR . 'images/');

// アプリケーション内で使用するDBに関する定数です。
define('DB_booking', 'booking');
define('DB_DEFAULT', DB_booking);

// 外部設定ファイルの内容を読み込みます。
// 使う側では「global $config;」を用います。
$config = parse_ini_file('app.ini', true);

// クラスファイルを読み込みます。
require_once APP_DIR . 'Dispatcher.php';
require_once COMMON_DIR . 'Common.php';
require_once COMMON_DIR . 'DataAccessException.php';
require_once CTRL_DIR . 'AbstractController.php';
require_once CTRL_DIR . 'ErrorController.php';
require_once MODEL_DIR . 'AbstractDao.php';
require_once VIEW_DIR . 'View.php';
require_once DB_DIR . 'DbFactory.php';
require_once DB_DIR . 'Db.php';
require_once DB_DIR . 'DbSqlite.php';

try {
	// 以降の処理を引き継ぎます。
	$dispatcher = new Dispatcher();
	$dispatcher->dispatch();
} catch (DataAccessException $dae) {
	// アプリケーション内で発生したDBアクセスに関するエラーはすべてここで捕まえます。
	new ErrorController($dae);
} catch (Throwable $e) {
	// アプリケーション内で発生したその他のエラーはすべてここで捕まえます。
	new ErrorController($e);
}
