<?php

/**
 * エラー発生時の共通コントローラー
 *
 * @access public
 */
class ErrorController extends AbstractController {

	/**
	 * コンストラクタ
	 *
	 * Viewにエラーオブジェクトを渡してレンダリングします。
	 *
	 * @access public
	 * @param Throwable $e エラー全般
	 */
	public function __construct(Throwable $e) {
		global $config;
		parent::__construct('error', 'index');
		$this->setupView();
		$this->view->assign('settings', $config['application']);
		$this->view->assign('e', $e);
		$this->view->render();
	}

}
