<?php

/**
 * データベース周りのエラーを集約
 *
 * @access public
 */
class DataAccessException extends RuntimeException {

	/**
	 * コンストラクタ
	 *
	 * 受け取った情報を元にmessageを設定します。
	 *
	 * @access public
	 * @param string $class エラーが発生したクラス名
	 * @param string $method エラーが発生したメソッド名
	 * @param string $message このアプリケーション向けにカスタマイズしたメッセージ
	 * @param Exception $e 他のExceptionをcatchしている場合はそのException
	 */
	public function __construct(
		string $class, string $method, string $message, Exception $e = null) {
		$timestamp = Common::formatDate('Y/m/d H:i:s');
		$this->message = "[{$timestamp}] [{$class}][{$method}] {$message}";
		$this->message .= $e ? '<hr />' . $e->message : '';
	}

}
