<?php

/**
 * 共通処理クラス
 *
 * @access public
 */
class Common {

	/**
	 * リクエストパラメータを取得します。
	 *
	 * POST/GETパラメータを取得し、存在しない場合は$defaultの値を返します。<br />
	 * 存在する場合はクォーテーション文字をエスケープして取得します。
	 *
	 * @access public static
	 * @param string $param_name 受け取るパラメータ名
	 * @param string $default パラメータが存在しないもしくは空文字の場合の戻り値、省略時空文字
	 * @return string 取得したパラメータの値
	 */
	public static function getParam(string $param_name, string $default = ''): string {
		$value = filter_input(INPUT_POST, $param_name) ?? filter_input(INPUT_GET, $param_name) ?? '';
		if ($value !== '') {
			$value = str_replace("\'", "'", $value);
			$value = str_replace('\"', '"', $value);
		} else {
			$value = $default;
		}
		return $value;
	}

	/**
	 * 改行文字をHTML改行タグへ変換、それ以外のタグ文字をエスケープします。
	 *
	 * @access public static
	 * @param string $string 変換対象文字列
	 * @return string 変換後文字列
	 */
	public static function nl2br(string $string): string {
		return str_replace("\n", '<br />', str_replace('<', '&lt;', str_replace('>', '&gt;', $string)));
	}

	/**
	 * 文字列を区切り文字で連結、いずれか空文字なら区切り文字なしで連結します。
	 *
	 * @access public static
	 * @param string $string_A 結合対象文字列
	 * @param string $string_B 結合文字列
	 * @param string $delim 区切り文字、省略時空文字
	 * @return string 結合後文字列
	 */
	public static function strJoin(string $string_A, string $string_B, string $delim = ''): string {
		if (($string_A == '') || ($string_B == '')) {
			$delim = '';
		}
		return $string_A . $delim . $string_B;
	}

	/**
	 * 右からn文字目を切り出します。
	 *
	 * 切り出し時の文字コードは「UTF-8」を使用します。
	 *
	 * @access public static
	 * @param string $string 切り出し対象文字列
	 * @param int $n 切り出し文字数
	 * @return string 切り出し後文字列
	 */
	public static function right(string $string, int $n): string {
		return mb_substr($string, ($n) * (-1), $n, "UTF-8");
	}

	/**
	 * 左からn文字目を切り出します。
	 *
	 * 切り出し時の文字コードは「UTF-8」を使用します。
	 *
	 * @access public static
	 * @param string $string 切り出し対象文字列
	 * @param int $n 切り出し文字数
	 * @return string 切り出し後文字列
	 */
	public static function left(string $string, int $n): string {
		return mb_substr($string, 0, $n, "UTF-8");
	}

	/**
	 * 時：分⇒分変換を行います。
	 *
	 * @access public static
	 * @param string $hourMinutes 変換対象の時：分
	 * @return int 変換後分（'1:30'⇒90）
	 */
	public static function hourMinuteToMinutes(string $hourMinutes): int {
		$return = 0;
		if ($hourMinutes != '') {
			$hm = explode(':', $hourMinutes);
			if (count($hm) == 2) {
				$return += ($hm[0] * 60) + ($hm[1] - 0);
			}
		}
		return $return;
	}

	/**
	 * 日付型変換を行います。
	 *
	 * 通常のdate関数ではサポートされない「曜日」を「w」で指定可能です。
	 *
	 * @access public static
	 * @param string $format PHPのdate関数への引数と同じ＋wで曜日指定可能
	 * @param string $date_string PHPのstrtotimeで変換可能な日付型文字列、省略時now
	 * @return string 変換後文字列
	 * @see http://php.net/manual/en/function.date.php
	 */
	public static function formatDate(string $format, string $date_string = 'now'): string {
		$result = '';
		if ($date_string !== null) {
			//曜日ローカライズ
			$weekdays = array('日', '月', '火', '水', '木', '金', '土');
			//曜日で分解
			$formats = explode('w', $format);
			//日付型取得
			$date = strtotime($date_string);
			//曜日を変換
			if (count($formats) > 0) {
				$w = $weekdays[date('w', $date)];
			}
			//曜日以外を変換
			foreach ($formats as & $format) {
				$format = date($format, $date);
			}
			//曜日で結合
			$result = implode($w, $formats);
		}
		return $result;
	}

	/**
	 * HTTPヘッダーセット（キャッシュ無）
	 *
	 * Expires: -1
	 * Last-Modified: 今
	 * Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0
	 * Pragma: no-cache
	 * Content-Type: text/html; Charset=UTF-8
	 *
	 * @access public static
	 */
	public static function setExpiresHeaders() {
		header('Expires: -1', true);
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT', true);
		header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0', true);
		header('Pragma: no-cache', true);
		header('Content-Type: text/html; Charset=UTF-8', true);
	}

	/**
	 * HTTPヘッダーセット（JSON用）
	 *
	 * Content-Type: text/javascript; Charset=UTF-8
	 *
	 * @access public static
	 */
	public static function setJsonHeaders() {
		header('Content-Type: text/javascript; Charset=UTF-8', true);
	}

	/**
	 * HTTPヘッダーセット（.csvダウンロード用）
	 *
	 * Content-Type: text/x-csv;
	 * Content-Disposition: attachment; filename=$filename.csv
	 *
	 * @access public static
	 * @param string $filename ダウンロードさせる.csvファイル名
	 */
	public static function setCsvHeaders(string $filename) {
		header('Content-Type: text/x-csv;');
		header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
	}

	/**
	 * HTTPヘッダーセット（.xlsダウンロード用）
	 *
	 * Cache-Control: max-age=10秒後
	 * Expires: 10秒後
	 * Pragma: cache
	 * Content-Type: application/vnd.ms-excel
	 * Content-Disposition: attachment; filename=$filename.xls
	 *
	 * @access public static
	 * @param string $filename ダウンロードさせる.xlsファイル名
	 */
	public static function setExcelHeaders(string $filename) {
		$cachetime = new DateTime('+10 second');
		header('Cache-Control: max-age=' . ($cachetime->format('U') - time()), true);
		header('Expires: ' . $cachetime->format(DATE_RFC1123), true);
		header('Pragma: cache', true);

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . $filename . '.xls"');
	}

	/**
	 * HTTPヘッダーセット（.docxダウンロード用）
	 *
	 * Cache-Control: max-age=10秒後
	 * Expires: 10秒後
	 * Pragma: cache
	 * Content-Type: application/vnd.ms-word
	 * Content-Disposition: attachment; filename=$filename.docx
	 *
	 * @access public static
	 * @param string $filename ダウンロードさせる.docxファイル名
	 */
	public static function setWordHeaders(string $filename) {
		$cachetime = new DateTime('+10 second');
		header('Cache-Control: max-age=' . ($cachetime->format('U') - time()), true);
		header('Expires: ' . $cachetime->format(DATE_RFC1123), true);
		header('Pragma: cache', true);

		header('Content-Type: application/vnd.ms-word');
		header('Content-Disposition: attachment; filename="' . $filename . '.docx"');
	}

}
