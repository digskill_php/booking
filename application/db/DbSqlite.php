<?php

class DbSqlite extends Db {

	private const NOW_STRING = "datetime('now','localtime')";

	public function __construct($dbpath) {
		$this->pdo = new PDO("sqlite:{$dbpath}");
		parent::__construct();
	}

	protected function getNowString(): string {
		return self::NOW_STRING;
	}

	protected function getLastInsertId(string $table): int {
		$sql = "select seq from sqlite_sequence where name = '{$table}'";
		$result = $this->query($sql);
		return $result[0]['seq'] - 0;
	}

	public function attachDb($dbname, $dbpath) {
		$sql = "attach database '{$dbpath}' as {$dbname};";
		return $this->exec($sql);
	}

}
